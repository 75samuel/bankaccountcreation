package com.hcl.spring.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.spring.bean.Account;


@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{
	
	/*@Query("select a from Account a where a.card.id = ?1")
	Account findByCard(long card_id);*/
	
	@Query("select a from Account a")
	List<Account>findAll();
	
	@Query("select a from Account a where a.id = ?1")
	Optional<Account> findById(String id);
	
	

}
