/*package com.hcl.spring.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.spring.bean.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Long>{
	
	@Query("select c from Card c where c.cardName = ?1")
	List<Card> findByName(String cardName);
	@Query("select c.id,c.cardName from Card c")
	List findAllCards();
	//List<Card> findAllCards();
	
	@Query("select c from Card c")
	List<Card>findAll();
	
	@Query("select c from Card c where c.id = ?1")
	Optional<Card> findById(String id);
	
}
*/