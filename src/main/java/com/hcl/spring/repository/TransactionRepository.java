package com.hcl.spring.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.spring.bean.Customer;
import com.hcl.spring.bean.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	@Query("select c from Customer c where c.name = ?1")
	List<Customer>findByName(String name);
	
	@Query("select c from Customer c")
	List<Transaction>findAll();
	
	@Query("select c from Transaction c where c.custId = ?1")
	Optional<Transaction> findByCustId(String custId);
	
	@Query("select c from Transaction c where c.accountNo = ?1")
	List<Transaction> findByAccNo(long accountNo);
	
	@Query("select c from Transaction c where YEAR(c.date) = ?1 and MONTH(c.date) = ?2")
	List<Transaction> findByMonth(int year,int month);
	/*
	@Query("insert into Customer(id,country,name) values(Customer.id,Customer.country,Customer.name ")
	Customer save(Customer customer);*/
}
 
