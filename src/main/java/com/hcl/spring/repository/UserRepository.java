package com.hcl.spring.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.hcl.spring.bean.Customer;

@Repository
public interface UserRepository extends JpaRepository<Customer, Long>{

	@Query("select c from Customer c where c.name = ?1")
	List<Customer>findByName(String name);
	
	@Query("select c from Customer c")
	List<Customer>findAll();
	
	@Query("select c from Customer c where c.id = ?1")
	Optional<Customer> findById(String id);
	/*
	@Query("insert into Customer(id,country,name) values(Customer.id,Customer.country,Customer.name ")
	Customer save(Customer customer);*/
}
 
