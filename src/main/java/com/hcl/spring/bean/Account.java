package com.hcl.spring.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="account")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Account {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long accountNo;
	
	@Column(name="amount")
	private double amount;
	
	
	
	
	
	
	@OneToOne  	
//	@JoinColumn(name="id")
	@JoinColumn(name = "cust_id")
	@JsonIgnoreProperties("account")
	private Customer customer;

	public long getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(long accountNo) {
		this.accountNo = accountNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	



	
	
	
	
	
}
