package com.hcl.spring.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="transaction")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Component
@SequenceGenerator(name="seqtrans", initialValue=1, allocationSize=100)

public class Transaction {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seqtrans")
	private long Id;
	
	@Column(name="accountNo")
	private long accountNo;
	
	@Column(name="custId")
	private long custId;
	@Column(name="withdraw")
	private double withdraw=0l;
	@Column(name="deposit")
	private double deposit=0l;
	@Column(name="balance")
	private double balance=0l;
	
	 
	private transient double amount;
	private transient long toAccNo=0L;


	@Column(name="comments")	
	private String comments;
	@Column(name="date")
	private Date date=new Date();


	public long getId() {
		return Id;
	}


	public void setId(long id) {
		Id = id;
	}


	 

	 

	public long getCustId() {
		return custId;
	}


	public void setCustId(long custId) {
		this.custId = custId;
	}


	public long getAccountNo() {
		return accountNo;
	}


	public void setAccountNo(long accountNo) {
		this.accountNo = accountNo;
	}


	public double getWithdraw() {
		return withdraw;
	}


	public void setWithdraw(double withdraw) {
		this.withdraw = withdraw;
	}


	public double getDeposit() {
		return deposit;
	}


	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}


	public double getBalance() {
		return balance;
	}


	public void setBalance(double balance) {
		this.balance = balance;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public long getToAccNo() {
		return toAccNo;
	}


	public void setToAccNo(long toAccNo) {
		this.toAccNo = toAccNo;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}
