/*package com.hcl.spring.bean;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="card")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Card {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="cardname")
	private String cardName;
	
	@Column(name="custname")
	
	private String custname;

	@Column(name="custid")
	private long custId;
	
	
	
	@Column(name="amount")
	private double amount=0.0d;
	
	@Column(name="user_id")
	private long user_id;
	
	//private Customer user2;

	@ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
	@JsonIgnoreProperties("cardList")
	
	private Customer customer;
	
	
	@OneToOne(mappedBy = "card", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
		      CascadeType.REFRESH })
	@JoinColumn(name = "account_account_no")
	@JsonIgnoreProperties("card")
	private Account account;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	

	public long getCustId() {
		return custId;
	}

	public void setCustId(long custId) {
		this.custId = custId;
	}

	
	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCustname() {
		return custname;
	}

	public void setCustname(String custname) {
		this.custname = custname;
	}

	
	

	
	
	
	public Account getAccount() {
		return account;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
    public String toString() {
        return "Card [id=" + id + "]";
    }
	
}
*/