package com.hcl.spring.controller;




import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.hcl.spring.bean.Account;
import com.hcl.spring.bean.Customer;
import com.hcl.spring.bean.Transaction;
import com.hcl.spring.repository.TransactionRepository;
import com.hcl.spring.service.AccountService;
import com.hcl.spring.service.UserService;

@RestController
@RequestMapping(value={"/customer"})
public class CustomerController {
	
	Logger logger = LoggerFactory.getLogger(CustomerController.class);
	@Autowired
	UserService userService;
	@Autowired
	AccountService accountService;
	@Autowired
	TransactionRepository transactionRepository;
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> getUserById(@PathVariable("id") long id) {
        System.out.println("Fetching User with id " + id);
        Optional<Customer> user = userService.findById(id);
        if (user == null) {
        	throw new CustomerNotfoundException();
         //   return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Customer>(user.get(), HttpStatus.OK);
    }
    
    @GetMapping(value = "/username/{name}", headers="Accept=application/json")
    public List<Customer> findByName(@PathVariable("name") String name) {
        System.out.println("Fetching User with name " + name);
        List<Customer> tasks = userService.findByName(name);
       
        return tasks;
    }
    
    @GetMapping(value = "/statementbyAccNo/{accNo}", headers="Accept=application/json")
    public List<Transaction> findByAccNo(@PathVariable("accNo") String accNo) {
        System.out.println("Fetching transaction with acctNo " + accNo);
        List<Transaction> trans = transactionRepository.findByAccNo(Long.parseLong(accNo));
       
        return trans;
    }
    
    @GetMapping(value = "/statementbyMonth/{month}", headers="Accept=application/json")
    public List<Transaction> findByDate(@PathVariable("month") String month) {
        System.out.println("Fetching transaction with month " + month);
        String my[]=month.split("-");
        System.out.println("year "+Integer.parseInt(my[0])+" month "+Integer.parseInt(my[1]));
        List<Transaction> trans = transactionRepository.findByMonth(Integer.parseInt(my[0]), Integer.parseInt(my[1])) ;
       
        return trans;
    }
	 @PostMapping(value="/create",headers="Accept=application/json")
	 public ResponseEntity<Void> createUser(@RequestBody Customer user, UriComponentsBuilder ucBuilder){
	     System.out.println("Creating User "+user.getName());
	     userService.createUser(user);
	     HttpHeaders headers = new HttpHeaders();
	     headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
	     return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	 }

	 @GetMapping(value="/get", headers="Accept=application/json")
	 public List<Customer> getAllUser() {	 
	  List<Customer> tasks=userService.getCustomers();
	  return tasks;
	
	 }

	@PutMapping(value="/update", headers="Accept=application/json")
	public ResponseEntity<String> updateUser(@RequestBody Customer currentUser)
	{
		System.out.println("sd");
	Optional<Customer> user = userService.findById(currentUser.getId());
	if (user==null) {
		throw new CustomerNotfoundException();
		//return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}
	userService.update(currentUser, currentUser.getId());
	return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	
	
	@PutMapping(value="/transfer/{accNo}", headers="Accept=application/json")
	public ResponseEntity<String> transferAmont(@RequestBody Transaction trans,@PathVariable("accNo") String accNo)
	{
		System.out.println("sd");
	Optional<Account> account = accountService.findById(Long.parseLong(accNo));
	if (account==null) {
		throw new AccountNotfoundException();
		//return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}else {
		if(account.get().getAmount()>=trans.getAmount()) {
			Account acctobj =account.get();
			acctobj.setAmount(acctobj.getAmount()-trans.getAmount());
			
			Optional<Account> account2=	accountService.findById(trans.getToAccNo());
			if(account2==null) {
				throw new AccountNotfoundException();
			}else {
				Account accountobj=account2.get();
				accountobj.setAmount(accountobj.getAmount()+trans.getAmount());
				//account.get().setAmount(account.get().getAmount()+trans.getAmount());
				trans.setCustId(acctobj.getCustomer().getId());
				trans.setAccountNo(Long.parseLong(accNo));
				trans.setWithdraw(trans.getAmount());
				trans.setDeposit(0);
				trans.setBalance(acctobj.getAmount());
				trans.setComments("Amount "+trans.getAmount()+" transfered to "+trans.getToAccNo());
				System.out.println("transaction1 "+transactionRepository.save(trans).getId());
				
				Transaction trans2 = new Transaction();
				trans2.setCustId(accountobj.getCustomer().getId());
				trans2.setAccountNo(accountobj.getAccountNo());
				trans2.setDeposit(trans.getAmount());
				trans2.setWithdraw(0);
				trans2.setBalance(accountobj.getAmount());
				trans2.setComments("Amount "+trans.getAmount()+" deposited from "+acctobj.getAccountNo());
				System.out.println("transaction2 "+transactionRepository.save(trans2).getId());
				
				accountService.update(accountobj);
				accountService.update(acctobj);
			//userService.update(custobj, custobj.getId());
			return new ResponseEntity<String>(HttpStatus.OK);
			}
			
		}
	}
	//userService.update(custobj, custobj.getId());
	return new ResponseEntity<String>(HttpStatus.NOT_ACCEPTABLE);
	}
	@DeleteMapping(value="/{id}", headers ="Accept=application/json")
	public ResponseEntity<Customer> deleteUser(@PathVariable("id") long id){
		Optional<Customer> user = userService.findById(id);
		if (user == null) {
			throw new CustomerNotfoundException();
		//	return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		}
		userService.deleteUserById(id);
		return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
	}
	
	@PatchMapping(value="/{id}", headers="Accept=application/json")
	public ResponseEntity<Customer> updateUserPartially(@PathVariable("id") long id, @RequestBody Customer currentUser){
		Optional<Customer> user = userService.findById(id);
		if(user ==null){
			throw new CustomerNotfoundException();
			//return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		}
		Customer usr =	userService.updatePartially(currentUser, id);
		return new ResponseEntity<Customer>(usr, HttpStatus.OK);
	}
}
