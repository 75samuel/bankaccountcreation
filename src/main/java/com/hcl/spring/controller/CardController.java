/*package com.hcl.spring.controller;




import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.hcl.spring.bean.Card;
import com.hcl.spring.service.CardService;

//import com.hcl.spring.bean.Customer;

@RestController
@RequestMapping(value={"/card"})
public class CardController {
	
	Logger logger = LoggerFactory.getLogger(CardController.class);
	@Autowired
	CardService cardService;
	
	
	 @GetMapping(value = "/cards", headers="Accept=application/json")
	    public List<?> findAllCards() {
	       logger.info("Fetching ALL  Cards " );
	        List<?> tasks = cardService.findAllCards();
	       
	        return tasks;
	    }
	 
	 
	  @GetMapping(value = "/username/{name}", headers="Accept=application/json")
	    public List<Customer> findByName(@PathVariable("name") String name) {
	        System.out.println("Fetching User with name " + name);
	        List<Customer> tasks = userService.findByName(name);
	       
	        return tasks;
	    }
	 
	 
	 @GetMapping(value = "/cardname/{cardName}", headers="Accept=application/json")
	    public List<Card> findByName(@PathVariable("cardName") String cardName) {
	        logger.info("Fetching User with name " + cardName);
	        List<Card> tasks = cardService.findByName(cardName);
	       
	        return tasks;
	    }
	
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Card> getUserById(@PathVariable("id") long id) {
        logger.info("Fetching User with id " + id);
        Optional<Card> card = cardService.findById(id);
        
        
        if (card.get() == null) {
        	logger.info("Card Not found for the Customer no "+id);
        	throw new CardNotfoundException();
        }
       logger.info("\n\n user get id "+card.get().getCustomer().getId());
       
        return new ResponseEntity<Card>(card.get(), HttpStatus.OK);
    }
    
	 @PostMapping(value="/create",headers="Accept=application/json")
	 public ResponseEntity<Void> createUser(@RequestBody Card card, UriComponentsBuilder ucBuilder){
	     logger.info("Creating Card "+card.getCardName());
	     logger.info("going to create ");
	     cardService.createCard(card);
	     HttpHeaders headers = new HttpHeaders();
	     headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(card.getId()).toUri());
	     return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	 }

	 @GetMapping(value="/get", headers="Accept=application/json")
	 public List<Card> getAllCard() {	 
	  List<Card> tasks=cardService.getCard();
	  return tasks;
	
	 }

	@PutMapping(value="/update", headers="Accept=application/json")
	public ResponseEntity<String> updateCard(@RequestBody Card currentUser)
	{
		
	Optional<Card> user = cardService.findById(currentUser.getId());
	if (user.get()==null) {
		logger.info("Card Not found for the card no "+currentUser.getId());
		throw new CardNotfoundException();
		//return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}
	cardService.update(currentUser, currentUser.getId());
	return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}", headers ="Accept=application/json")
	public ResponseEntity<Card> deleteUser(@PathVariable("id") long id){
		Optional<Card> user = cardService.findById(id);
		if (user.get() == null) {
			logger.info("Card Not found for the card no "+id);
			throw new CardNotfoundException();
			//return new ResponseEntity<Card>(HttpStatus.NOT_FOUND);
		}
		cardService.deleteUserById(id);
		return new ResponseEntity<Card>(HttpStatus.NO_CONTENT);
	}
	
	@PatchMapping(value="/{id}", headers="Accept=application/json")
	public ResponseEntity<Card> updateUserPartially(@PathVariable("id") long id, @RequestBody Customer currentUser){
		Card user = cardService.findById(id);
		if(user ==null){
			return new ResponseEntity<Card>(HttpStatus.NOT_FOUND);
		}
		Card usr =	cardService.updatePartially(currentUser, id);
		return new ResponseEntity<Card>(usr, HttpStatus.OK);
	}
}
*/