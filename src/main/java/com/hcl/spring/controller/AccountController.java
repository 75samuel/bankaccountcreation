package com.hcl.spring.controller;




import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.hcl.spring.bean.Account;
import com.hcl.spring.service.AccountService;



@RestController
@RequestMapping(value={"/account"})
public class AccountController {
	
	Logger logger = LoggerFactory.getLogger(AccountController.class);
	@Autowired
	AccountService accountService;
	
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> getUserById(@PathVariable("id") long id) {
        logger.info("Fetching User with id " + id);
        Optional<Account> card = accountService.findById(id);
        if (!card.isPresent() ) {
        	logger.info("Account Not found for the Account no "+id);
        	throw new AccountNotfoundException();
           // return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
        }
       logger.info("\ncustomer get id "+card.get().getCustomer().getId());
       
        return new ResponseEntity<Account>(card.get(), HttpStatus.OK);
    }
    
    
    
    
    /*@GetMapping(value = "/card/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> getAccountByCardId(@PathVariable("id") long id) {
        logger.info("Fetching Account with card No :" + id);
        Account card = accountService.findByCard(id);
        if (card == null) {
        	logger.info("Account Not found for the Card no "+id);
        	throw new AccountNotfoundException();
          //  return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
        }
       logger.info("\n\n customer get id "+card..getId());
       
        return new ResponseEntity<Account>(card, HttpStatus.OK);
    }*/
    
	 @PostMapping(value="/create",headers="Accept=application/json")
	 public ResponseEntity<Void> createAccount(@RequestBody Account user, UriComponentsBuilder ucBuilder){
	     System.out.println("Creating Account "+user.getAccountNo());
	     accountService.createAccount(user);
	     HttpHeaders headers = new HttpHeaders();
	     headers.setLocation(ucBuilder.path("/account/{id}").buildAndExpand(user.getAccountNo()).toUri());
	     logger.info("Account is created successfully ");
	     return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	 }

	 @GetMapping(value="/get", headers="Accept=application/json")
	 public List<Account> getAllAccount() {	 
	  List<Account> tasks=accountService.getAccounts();
	  return tasks;
	
	 }

	@PutMapping(value="/update", headers="Accept=application/json")
	public ResponseEntity<String> updateAccount(@RequestBody Account currentUser)
	{
		
	Optional<Account> user = accountService.findById(currentUser.getAccountNo());
	if (user==null) {
		logger.info("Account Not found for the Account no "+currentUser.getAccountNo());
		throw new AccountNotfoundException();
		//return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}
	accountService.update(currentUser );
	return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}", headers ="Accept=application/json")
	public ResponseEntity<Account> deleteUser(@PathVariable("id") long id){
		Optional<Account> user = accountService.findById(id);
		if (user == null) {
			logger.info("Account Not found for the Account no "+id);
			throw new AccountNotfoundException();
		//	return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		}
		//accountService.deleteUserById(id);
		return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
	}
	
	/*@PatchMapping(value="/{id}", headers="Accept=application/json")
	public ResponseEntity<Card> updateUserPartially(@PathVariable("id") long id, @RequestBody Customer currentUser){
		Card user = accountService.findById(id);
		if(user ==null){
			return new ResponseEntity<Card>(HttpStatus.NOT_FOUND);
		}
		Card usr =	accountService.updatePartially(currentUser, id);
		return new ResponseEntity<Card>(usr, HttpStatus.OK);
	}*/
}
