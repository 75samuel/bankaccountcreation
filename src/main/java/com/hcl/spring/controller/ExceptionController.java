package com.hcl.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
	
	Logger logger = LoggerFactory.getLogger(ExceptionController.class);
   @ExceptionHandler(value = CustomerNotfoundException.class)
   public ResponseEntity<Object> exception(CustomerNotfoundException exception) {
	   logger.debug("Customer not found");
      return new ResponseEntity<>("Customer not found", HttpStatus.NOT_FOUND);
   }
   
   @ExceptionHandler(value = CardNotfoundException.class)
   public ResponseEntity<Object> exception(CardNotfoundException exception) {
	   logger.debug("Card not found");
      return new ResponseEntity<>("Card not found", HttpStatus.NOT_FOUND);
   }
   
   @ExceptionHandler(value = AccountNotfoundException.class)
   public ResponseEntity<Object> exception(AccountNotfoundException exception) {
	   logger.debug("Account not found");
      return new ResponseEntity<>("Account not found", HttpStatus.NOT_FOUND);
   }
   
   @ExceptionHandler(value = Exception.class)
   public ResponseEntity<Object> exception(Exception exception) {
	   logger.debug("Exception Occures :"+exception);
      return new ResponseEntity<>("Exception occures "+exception, HttpStatus.BAD_REQUEST);
   }
}