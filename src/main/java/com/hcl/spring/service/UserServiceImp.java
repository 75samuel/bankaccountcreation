package com.hcl.spring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.spring.bean.Account;
import com.hcl.spring.bean.Customer;
import com.hcl.spring.bean.CustomerDetails;
import com.hcl.spring.bean.Transaction;
import com.hcl.spring.repository.TransactionRepository;
import com.hcl.spring.repository.UserRepository;

@Service
@Transactional
public class UserServiceImp implements UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	AccountServiceImp accountServiceImp;
	@Autowired
	TransactionRepository transactionRepository;
	@Autowired
	 
	Transaction transaction;
	
	
	public void createUser(Customer user) {
		// TODO Auto-generated method stub
		System.out.println("Record going to insertd");
		System.out.println("id "+user.getId());
		System.out.println("country "+user.getCountry());
		
		
		Account account=user.getAccount();
		account.setAmount(10000.00);
		account.setCustomer(user);
		accountServiceImp.createAccount(account);
		
		 
		transaction = new Transaction();
		transaction.setAccountNo(userRepository.save(user).getAccount().getAccountNo());
		transaction.setDeposit(account.getAmount());
		transaction.setBalance(account.getAmount());
		transaction.setComments("account is created for custId "+user.getId()+" and accountNo "+account.getAccountNo()+ " with initial amount "+account.getAmount());
		System.out.println("transaction id "+transactionRepository.save(transaction).getId());
		
	}

	public List<Customer> getCustomers() {
		// TODO Auto-generated method stub
		List<Customer> list = new ArrayList<>();
		//Lambda expression
		userRepository.findAll().forEach(e -> list.add(e)); 
		return list;
	}
	
	
	
	public List<Customer> findByName(String name) {
		// TODO Auto-generated method stub
		List<Customer> list = new ArrayList<>();
		//Lambda expression
		userRepository.findByName(name).forEach(e -> list.add(e)); 
		return list;
	}
	

	public Optional<Customer> findById(long id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id);
	}

	public Customer update(Customer user, long l) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	public void deleteUserById(long id) {
		// TODO Auto-generated method stub
		userRepository.deleteById(id);
	}

	public Customer updatePartially(Customer user, long id) {
		// TODO Auto-generated method stub
		Optional<Customer> usr = findById(id);
		if(usr!=null)
		usr.get().setCountry(user.getCountry());
		return userRepository.save(usr.get());
	}



}
