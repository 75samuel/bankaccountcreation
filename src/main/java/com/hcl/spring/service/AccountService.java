package com.hcl.spring.service;

import java.util.List;
import java.util.Optional;

import com.hcl.spring.bean.Account;

import com.hcl.spring.bean.Customer;

public interface AccountService {
	public void createAccount(Account account);
	public List<Account> getAccounts();
	public Optional<Account> findById(long id);
	//Account findByCard(long card_id);
	public Account update(Account account );
//	public void deleteAccountById(long id);
//	public Card updatePartially(Card card, long id);
}
