package com.hcl.spring.service;

import java.util.List;
import java.util.Optional;

import com.hcl.spring.bean.Customer;

public interface UserService {
	public void createUser(Customer user);
	public List<Customer> getCustomers();
	public Optional<Customer> findById(long id);
	public Customer update(Customer user, long l);
	public void deleteUserById(long id);
	public Customer updatePartially(Customer user, long id);
	public List<Customer>findByName(String name);
}
