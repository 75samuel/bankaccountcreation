/*package com.hcl.spring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.spring.bean.Card;
import com.hcl.spring.bean.Customer;
import com.hcl.spring.bean.CustomerDetails;
import com.hcl.spring.repository.CardRepository;
import com.hcl.spring.repository.UserRepository;

@Service
@Transactional
public class CardServiceImp implements CardService {
	@Autowired
	CardRepository cardRepository;
	@Autowired
	UserRepository userRepository;
	
	public void createCard(Card card) {
		// TODO Auto-generated method stub
	 Customer user=	card.getCustomer();
	 System.out.println("User id "+user.getId());
	 if(user!=null) {
		 if(user.getId()>0) {
			card.setCustomer(userRepository.getOne(user.getId())) ;
		 }
	 }
		cardRepository.save(card);
	}

	public List<Card> getCard() {
		// TODO Auto-generated method stub
		List<Card> list = new ArrayList<>();
		//Lambda expression
		cardRepository.findAll().forEach(e -> list.add(e));
		
 
		return list;
	}
	
	
	public List<Card> findAllCards(){
		List list = new ArrayList<>();
		System.out.println("\n\n\n findAllCards \n\n\n");
		cardRepository.findAllCards().forEach(e->list.add(e));
		
		System.out.println("\n\n\n findAllCards \n\n\n"+cardRepository.findAllCards());
	 return	list;
	}
	
	public List<Card> findByName(String name) {
		// TODO Auto-generated method stub
		List<Card> list = new ArrayList<>();
		//Lambda expression
		cardRepository.findByName(name).forEach(e -> list.add(e)); 
		return list;
	}
	

	public Optional<Card> findById(long id) {
		// TODO Auto-generated method stub
		return cardRepository.findById(id);
	}

	public Card update(Card user, long l) {
		// TODO Auto-generated method stub
		return cardRepository.save(user);
	}

	public void deleteUserById(long id) {
		// TODO Auto-generated method stub
		cardRepository.deleteById(id);
	}

	public Customer updatePartially(Customer user, long id) {
		// TODO Auto-generated method stub
		Card card = findById(id);
		card.setCountry(user.getCountry());
		return userRepository.save(usr);
	}



}
*/