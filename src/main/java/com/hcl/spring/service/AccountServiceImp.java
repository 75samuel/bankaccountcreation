package com.hcl.spring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.spring.bean.Account;

import com.hcl.spring.bean.Customer;
import com.hcl.spring.bean.CustomerDetails;
import com.hcl.spring.repository.AccountRepository;
//import com.hcl.spring.repository.CardRepository;
import com.hcl.spring.repository.UserRepository;

@Service
@Transactional
public class AccountServiceImp implements AccountService {
	@Autowired
	AccountRepository accountRepository;
	
	
	public void createAccount(Account account) {
		/*Card card=null;
		Card ccard= account.getCard();
		if(ccard!=null) {
			if(ccard.getId()>0) {
				System.out.println(ccard.getId());
				card=cardrepository.getOne(ccard.getId());
				account.setCard(card);
			}
			
		}	*/	
		// TODO Auto-generated method stub
	accountRepository.save(account);
		System.out.println("Account is created");
		
		
	}

	public List<Account> getAccounts() {
		// TODO Auto-generated method stub
		List<Account> list = new ArrayList<>();
		//Lambda expression
		accountRepository.findAll().forEach(e -> list.add(e));
		
 
		return list;
	}
	
	
	
	/*public Account findByCard(long name) {
		// TODO Auto-generated method stub
		//List<Card> list = new ArrayList<>();
		//Lambda expression
	Account account=	accountRepository.findByCard(name);
		return account;
	}*/
	

	public Optional<Account> findById(long id) {
		// TODO Auto-generated method stub
		return accountRepository.findById(id);
	}

	public Account update(Account user ) {
		// TODO Auto-generated method stub
		return accountRepository.save(user);
	}



	/*public void deleteUserById(long id) {
		// TODO Auto-generated method stub
		cardRepository.delete(id);
	}*/

	/*public Customer updatePartially(Customer user, long id) {
		// TODO Auto-generated method stub
		Card card = findById(id);
		card.setCountry(user.getCountry());
		return userRepository.save(usr);
	}*/



}
