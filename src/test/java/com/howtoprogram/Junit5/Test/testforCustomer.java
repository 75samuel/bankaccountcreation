package com.howtoprogram.Junit5.Test;

import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.hcl.spring.bean.Account;
import com.hcl.spring.bean.Customer;

@TestInstance(Lifecycle.PER_CLASS)
class testforCustomer {

	 
	private Customer customer;
	private Account account;
	long i;
	String name;

	@BeforeAll
	public void init() {

		customer = new Customer();
		account = new Account();
		customer.setId(100000000000000001L);
		i = 100000000000000001L;
		name = "Samuel";
		account.setAccountNo(12345678910L);
		account.setAmount(10000);
		// customer.setId(1);
		customer.setName("samuel");
		customer.setAccount(account);
		System.out.println("BeforeAll executed");
	}

	@AfterAll
	public  void tearDown() {
		customer = null;
		account = null;
	}

	@Test
	// @Order(2)
	public void testCustomerAccount() {
		assertEquals(account, customer.getAccount());
		System.out.println("Test case for customer acount tested");

	}

	@Test
	// @Order(1)
	@DisplayName("Customer Id")
	public void testCustomerId() {
		assertEquals(i, customer.getId());
		System.out.println("Test case for customer id tested");
	}

	@Test
	// @Order(3)
	public void testCustomerName() {

		assertEquals(name.toLowerCase(), customer.getName().toLowerCase());
		System.out.println("Test case for customer name tested");

	}

}
